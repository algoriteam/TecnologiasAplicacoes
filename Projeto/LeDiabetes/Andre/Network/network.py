# Imports
import glob
import warnings
import numpy as np
import matplotlib.pyplot as plt
import prepare_dataset as pd

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers import Reshape
from keras.layers import Activation
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import np_utils
from keras import backend as K

# Image ordering settings
K.set_image_dim_ordering('th') #pode ser 'th' ou 'tf'

class LE_DIABETES:

	# ----- VARIABLES -----

	# Network settings
	'''
		0 - Desconhecido
		1 - Granulacao
		2 - Cicatrização
		3 - Necrose
	'''
	num_classes = 4

	# Training and testing data
	width = 200
	height = 200

	# Etapa 1 - Definir a topologia da rede (arquitectura do modelo) e compilar
	def create_compile_model(self):
		# Criar modelo
		model = Sequential()

		'''
		CNN-PLUS:
			model.add(Conv2D(30, (5, 5), input_shape=(3, self.height, self.width), activation='relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))
			model.add(Conv2D(15, (3, 3), activation='relu'))
			model.add(MaxPooling2D(pool_size=(2, 2)))

			model.add(Dropout(0.2))
			model.add(Flatten())

			model.add(Dense(128, activation='relu'))
			model.add(Dense(50, activation='relu'))
			model.add(Dense(self.num_classes, activation='softmax'))
		'''

		# Compile model
		model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
		return model

	# Etapa 2 - Preparar dataset
	def prepare_dataset(self, use_pre_classified_data):
		prepare = pd.DATA_PREPARATION()

		if (use_pre_classified_data):
			(X_train_int, Y_train_int), (X_test_int, Y_test_int) = prepare.fetch_prepare_dataset()
		else:
			(X_train_int, Y_train_int), (X_test_int, Y_test_int) = prepare.load_prepare_dataset(False, False)

		X_train_np = np.array(X_train_int)
		X_test_np = np.array(X_test_int)

		Y_train = np.array(Y_train_int)
		Y_test = np.array(Y_test_int)

		# Transformar para o formato [instancias][pixeis][largura][altura]
		X_train = X_train_np.reshape(X_train_np.shape[0], 3, self.width, self.height).astype('float32')
		X_test = X_test_np.reshape(X_test_np.shape[0], 3, self.width, self.height).astype('float32')

		# Normalizar os valores dos pixeis de 0-255 para 0-1
		X_train = X_train / 255
		X_test = X_test / 255

		# Transformar o label que é um inteiro em categorias binárias, o valor passa a ser o correspondente à posição
		# o 5 passa a ser a lista [0. 0. 0. 0. 0. 1. 0. 0. 0. 0.]
		for y_r in Y_train:
			for l in y_r:
				l = np_utils.to_categorical(l)

		for y_t in Y_test:
			for l in y_t:
				l = np_utils.to_categorical(l)

		return (X_train, Y_train), (X_test, Y_test)

	# Etapa 3 - Treinar a rede
	def train_network(self, model, X_train, Y_train, X_test, Y_test):
		# treinar a rede
		model.fit(X_train, Y_train, validation_data=(X_test, Y_test), epochs=10, batch_size=200, verbose=2)

		# avaliação final com os casos de teste
		scores = model.evaluate(X_test, y_test, verbose=0)
		print('Scores: ', scores)
		print("Erro modelo MLP: %.2f%%" % (100-scores[1]*100))

if __name__ == '__main__':
	with warnings.catch_warnings():
		warnings.simplefilter("ignore")
		d = LE_DIABETES()

		print('\n-----------------------------> [STAGE 1] -> Prepare DATASET\n')
		# Etapa 2 - Preparar dataset
		(X_train, Y_train), (X_test, Y_test) = d.prepare_dataset(True)
		print('\n-----------------------------> [STAGE 1] -> DONE!\n')

		print('\n-----------------------------> [STAGE 2] -> Create and compile LeDiabetes NETWORK\n')
		# Etapa 1 - Criar e compilar modelo da rede
		model = d.create_compile_model()
		print('\n-----------------------------> [STAGE 2] -> DONE!\n')

		print('\n-----------------------------> [STAGE 3] -> Train and test LeDiabetes NETWORK\n')
		# Etapa 3 - Treinar a rede
		d.train_network(model, X_train, Y_train, X_test, Y_test)
		print('\n-----------------------------> [STAGE 3] -> DONE!\n')
