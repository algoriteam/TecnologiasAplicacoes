# Imports
import os
import sys
import warnings
import time
import glob
import json
import numpy as np
import matplotlib.pyplot as plt

from scipy import ndimage as ndi
from skimage import io, img_as_ubyte, color, transform, filters, future, segmentation
from skimage.future import graph

class DATA_PREPARATION:

	# ----- VARIABLES -----

	slic_compactness = 10
	slic_segments = 2048
	normalize_threshold = 13

	training_percentage = 0.66
	testing_percentage = 0.33

	# PELE
	target_pele = {
		"Pele0" : (150, 96, 47), #96602F
		"Pele1" : (198, 147, 128), #C69380
		"Pele2" : (246, 203, 167), #F6CBA7
		"Pele3" : (236, 170, 141), #ECAA8D
		"Pele4" : (170, 104, 110), #AA686E
		"Pele5" : (115, 90, 73), #735A49
		"Pele6" : (89, 54, 15), #59360F
		"Pele7" : (135, 79, 53), #874F35
		"Pele8" : (185, 132, 120), #B98478
		"Pele9" : (152, 111, 86) #986F56
	}

	# GRANULACAO
	target_granulacao = {
		"Granulacao1" : (231, 111, 138), #E76F8A
		"Granulacao2" : (163, 69, 72), #A34548
		"Granulacao3" : (161, 32, 27), #A1201B
		"Granulacao4" : (110, 31, 23), #6E1F17
		"Granulacao5" : (200, 114, 118), #C47276
		"Granulacao6" : (175, 104, 71), #AF6847
		"Granulacao7" : (82, 20, 21) #521415
	}

	# CICATRIZ
	target_cicatriz = {
		"Cicatriz1" : (219, 212, 202), #C5A25B
		"Cicatriz2" : (132, 79, 14), #844F0E
		"Cicatriz3" : (176, 148, 32), #B09420
		"Cicatriz4" : (198, 147, 60), #C6933C
		"Cicatriz5" : (172, 86, 43), #AC562B
		"Cicatriz6" : (113, 76, 49), #714C31
		"Cicatriz7" : (192, 179, 106), #C0B36A
		"Cicatriz8" : (93, 72, 55), #5D4837
		"Cicatriz9" : (123, 95, 42) #7B5F2A
	}

	# NECROSE
	target_necrose = {
		"Necrose1" : (69, 35, 17), #452311
		"Necrose2" : (39, 15, 8), #270F08
		"Necrose3" : (92, 35, 29), #5C231D
		"Necrose4" : (89, 74, 72), #594A48
		"Necrose5" : (91,56,28) #5B381C
	}

	# CLASSIFICATION COLORS
	target_colors = { **target_pele, **target_granulacao, **target_cicatriz, **target_necrose }

	represent_colors = {
		"Pele": (255, 255, 255),
		"Granulacao": (255, 0, 0), # Vermelho
		"Cicatriz": (0, 0, 255), # Azul
		"Necrose": (0, 0, 0) # Preto
	}

	label_colors = {
		"Pele": 0,
		"Granulacao": 1,
		"Cicatriz": 2,
		"Necrose": 3
	}

	# ----- SEGMENTATION -----

	def classify_image_slic(self, image):
		# Referências e exemplos:
		'''
		Slic:
		http://scikit-image.org/docs/dev/api/skimage.segmentation.html#skimage.segmentation.slic
		Radhakrishna Achanta, Appu Shaji, Kevin Smith, Aurelien Lucchi, Pascal Fua, and Sabine Süsstrunk,
		SLIC Superpixels Compared to State-of-the-art Superpixel Methods, TPAMI, May 2012.
		http://ivrg.epfl.ch/research/superpixels#SLICO
		'''
		# Processar segmentação por utilização do algoritmo "slic"
		slic_labels = segmentation.slic(image, compactness=self.slic_compactness, n_segments=self.slic_segments)

		return slic_labels

	def normalized_cut(self, image, labels):
		# Referências e exemplos:
		'''
		Normalized Cut:
			http://scikit-image.org/docs/dev/auto_examples/segmentation/plot_ncut.html
		'''
		# Normalizar valores obtidos anteriormente
		g = graph.rag_mean_color(image, labels)
		normalized_labels = future.graph.cut_threshold(labels, g, self.normalize_threshold)

		return normalized_labels

	def classify_image_manual(self, image, normalized_output):
		reshape_x = image.shape[0]
		reshape_y = image.shape[1]

		# Processar labels fazendo verificação simples e manual pixel por pixel
		labels = np.zeros((reshape_x, reshape_y), dtype=np.int8)
		original_temp = image.copy()
		normalized_temp = normalized_output.copy()

		for i in range(reshape_x):
			for j in range(reshape_y):
				differences = [[self.color_difference(normalized_temp[i, j], target_value), target_name] for target_name, target_value in self.target_colors.items()]
				differences.sort()
				color_name = differences[0][1]
				color_general = color_name[:-1]

				# Check label for color
				labels[i][j] = self.label_colors[color_general]

				# Highlight da zona se não for PELE
				if (color_general != "Pele"):
					original_temp[i][j] = self.represent_colors[color_general]

		return (original_temp, labels)

	# ----- AUXILIARY -----

	def color_difference(self, a, b):
		return sum([abs(a - b) for a, b in zip(a, b)])

	def plot_images(self, image, slic_output, normalized_output, classified_image):
		# Construir gráficos
		fig, axes = plt.subplots(nrows=2, ncols=2, figsize=(8, 8), sharex=True, sharey=True)
		ax = axes.ravel()

		ax[0].imshow(image, interpolation='nearest')
		ax[0].set_title("Original")

		ax[1].imshow(slic_output, interpolation='nearest')
		ax[1].set_title("Slic")

		ax[2].imshow(normalized_output, interpolation='nearest')
		ax[2].set_title("Normalized")

		ax[3].imshow(image, interpolation='nearest')
		ax[3].imshow(classified_image, interpolation='nearest', alpha=0.5)
		ax[3].set_title("Classified")

		for a in ax:
			a.axis('off')

		fig.tight_layout()
		plt.show()

	def save_to_file(self, data, filename):
		with open(filename, 'w') as outfile:
		    json.dump(data, outfile)

	# ----- MAIN -----

	def fetch_prepare_dataset(self, reshape_x, reshape_y, image_format, input_images_path, output_images_path):
		folder_len = len(output_images_path)
		extension_len = len("label") # 5

		print('\n> [LOADING DATASET]\n')

		output_directory = glob.glob(output_images_path + "*.label")
		print('     Input directory: "%s"' % input_images_path)
		print('     Output directory: "%s"' % output_images_path)

		count = len(output_directory)
		print('     Labels count: "%d"' % count)
		print('     Image extension: "%s"' % image_format)

		X_images, Y_labels = [] , []

		X_train, Y_train = [], []
		X_test, Y_test = [], []
		print('\n> [PROGRESS]\n')

		for label in output_directory:
			label_name = label[folder_len:-(extension_len + 1)]

			print('     [Label "%s"]' % label_name, end='', flush=True)

			print(' -> Read', end='', flush=True)
			# Ler conteudo da imagem
			image_pixels = io.imread(input_images_path + label_name + "." + image_format)

			with open(label, 'r') as infile:
				image_labels_temp = json.load(infile)
				image_labels = image_labels_temp

			if (len(image_pixels) > 0):
				print(' -> Resize', end='', flush=True)
				# Realizar resize da imagem
				reshaped_pixels = transform.resize(image_pixels, (reshape_x, reshape_y, 3), mode='reflect')

				print(' -> Convert', end='', flush=True)
				# Converter de float64->int8
				image_converted = img_as_ubyte(reshaped_pixels)

				# Adicionar à lista de imagens e labels
				X_images.append(image_converted)
				Y_labels.append(image_labels)

				print(', Done!')
			else:
				print(' -> No correspondent image found!')

		i = 0
		count = len(X_images)
		print('\n> [SPLITTING THE DATASET]\n')
		print('     Training percentage: "%.2f"' % self.training_percentage)
		print('     Testing percentage: "%.2f"' % self.testing_percentage)

		for i in range(count):
			image = X_images[i]
			label = Y_labels[i]
			med_value = i / count

			if (med_value <= self.training_percentage):
				X_train.append(image)
				Y_train.append(label)
			else:
				X_test.append(image)
				Y_test.append(label)

		print('\n> [PREPARATION FINISHED]')
		return (X_train, Y_train), (X_test, Y_test)

	def load_prepare_dataset(self, debug, save, reshape_x, reshape_y, image_format, input_images_path, output_images_path):
		folder_len = len(input_images_path)
		extension_len = len(image_format)

		print('\n> [LOADING AND PREPARING DATASET]\n')

		input_directory = glob.glob(input_images_path + "*." + image_format)
		print('     Input directory: "%s"' % input_images_path)
		print('     Output directory: "%s"' % output_images_path)

		count = len(input_directory)
		print('     Image count: "%d"' % count)
		print('     Image extension: "%s"' % image_format)

		X_images, Y_labels = [] , []

		X_train, Y_train = [], []
		X_test, Y_test = [], []
		print('\n> [CLASSIFICATION]\n')

		for image in input_directory:
			try:
				image_name = image[folder_len:-(extension_len + 1)]

				# Tempo de execução - Início
				start = time.time()

				print('     [Image "%s"]' % image_name, end='', flush=True)
				print(' -> Read', end='', flush=True)
				# Ler conteudo da imagem
				image_pixels = io.imread(image)

				print(' -> Resize', end='', flush=True)
				# Realizar resize da imagem
				reshaped_pixels = transform.resize(image_pixels, (reshape_x, reshape_y, 3), mode='reflect')

				print(' -> Convert', end='', flush=True)
				# Converter de float64->int8
				image_converted = img_as_ubyte(reshaped_pixels)

				print(' -> Superpixelize', end='', flush=True)
				# Realizar segmentação por utilização do algoritmo "slic"
				slic_labels = self.classify_image_slic(image_converted)
				slic_output = color.label2rgb(slic_labels, image_converted, kind='avg')

				print(' -> Normalize', end='', flush=True)
				# Normalizar segmentação de forma a aproximar a cor dos vários pedaços obtidos durante o processo de segmentação
				normalized_labels = self.normalized_cut(image_converted, slic_labels)
				normalized_output = color.label2rgb(normalized_labels, image_converted, kind='avg')

				print(' -> Classify', end='', flush=True)
				# Manual classify
				(classified_image, final_labels) = self.classify_image_manual(image_converted, normalized_output)

				if (save == True):
					print(' -> Save', end='', flush=True)
					# Salvar labels da imagem classificada
					self.save_to_file(final_labels.tolist(), output_images_path + image[folder_len:-extension_len] + "label")

				# Tempo de execução - Fim
				end = time.time()

				if (debug == True):
					# Realizar plot dos resultados obtidos
					print(' -> Plot', end='', flush=True)
					self.plot_images(image_converted, slic_output, normalized_output, classified_image)

				# Mostrar tempo de execução
				print(', Done in %.2f s!' % (end - start))

				# Adicionar à lista de imagens e labels
				X_images.append(image_converted)
				Y_labels.append(final_labels)
			except:
				print(' -> There was an exception!')
				pass

		i = 0
		count = len(X_images)
		print('\n> [SPLITTING THE DATASET]\n')
		print('     Training percentage: "%.2f"' % self.training_percentage)
		print('     Testing percentage: "%.2f"' % self.testing_percentage)

		for i in range(count):
			image = X_images[i]
			label = Y_labels[i]
			med_value = i / count

			if (med_value <= self.training_percentage):
				X_train.append(image)
				Y_train.append(label)
			else:
				X_test.append(image)
				Y_test.append(label)

		print('\n> [PREPARATION FINISHED]')
		return (X_train, Y_train), (X_test, Y_test)
