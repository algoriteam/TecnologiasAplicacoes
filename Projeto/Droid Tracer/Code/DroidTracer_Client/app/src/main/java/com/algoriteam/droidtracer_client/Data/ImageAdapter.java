package com.algoriteam.droidtracer_client.Data;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.algoriteam.droidtracer_client.Presentation.Menu;
import com.algoriteam.droidtracer_client.R;
import com.algoriteam.droidtracer_client.Structure.FileTreeNode;

import java.io.File;
import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {

    // ----- VARIABLES -----

    private FragmentActivity activity;
    private ImageAdapter self;
    private Context context;
    private ArrayList<String> images;

    // ----- CONSTRUCTOR -----

    public ImageAdapter(FragmentActivity a, Context c, ArrayList<String> i) {
        activity = a;
        self = this;
        context = c;
        images = i;
    }

    // ----- METHODS -----

    public int getCount() {
        return images.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // Create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View res = inflater.inflate(R.layout.grid_imageitem, null);

        final String imagePath = images.get(position);
        final File tempFile = new File(imagePath);
        Bitmap tempBitmap = BitmapFactory.decodeFile(imagePath);

        // Set image
        ImageView image = (ImageView) res.findViewById(R.id.grid_item_image);
        image.setImageBitmap(tempBitmap);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(imagePath)), "image/*");
                activity.startActivity(intent);
            }
        });

        // Set delete button
        FloatingActionButton delete = (FloatingActionButton) res.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            // UI THREAD
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("Do you want to delete the image?");

                // Set up the buttons
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        images.remove(position);
                        self.notifyDataSetChanged();
                        tempFile.delete();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                }
            });
            }
        });

        return res;
    }

}
