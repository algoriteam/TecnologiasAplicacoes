package com.algoriteam.droidtracer_client.Data;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.algoriteam.droidtracer_client.R;
import com.algoriteam.droidtracer_client.Structure.FileTreeNode;

public class FileAdapter extends BaseAdapter {

    // ----- VARIABLES -----

    private Context context;
    private FileTreeNode tree;

    // ----- CONSTRUCTOR -----

    public FileAdapter(Context c, FileTreeNode t) {
        context = c;
        tree = t;
    }

    // ----- METHODS -----

    public int getCount() {
        return tree.getNodes().size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // Create a new ImageView and TextView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View res = inflater.inflate(R.layout.grid_fileitem, null);

        FileTreeNode node = tree.getNodes().get(position);

        // Set image
        ImageView image = (ImageView) res.findViewById(R.id.grid_item_image);

        if (node.isFile())
            image.setImageResource(R.drawable.ic_file);
        else
            image.setImageResource(R.drawable.ic_folder);

        // Set label
        TextView text = (TextView) res.findViewById(R.id.grid_item_label);
        text.setText(node.getName());

        return res;
    }

}
