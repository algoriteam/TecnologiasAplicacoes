package com.algoriteam.droidtracer_client.Presentation;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Looper;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.algoriteam.droidtracer_client.Server.RMIServerInterface;
import com.algoriteam.droidtracer_client.RMI.RpcClient;
import com.algoriteam.droidtracer_client.R;

import java.net.SocketException;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.UUID;

public class Menu extends AppCompatActivity {

    // ----- VARIABLES -----

    // MENU
    private static final int REQUEST_PERMISSIONS = 1;
    private static final String[] PERMISSIONS = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.INTERNET,
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.ACCESS_WIFI_STATE,
        Manifest.permission.CHANGE_WIFI_STATE
    };

    private Button connectButton;

    // CLIENT
    public static RMIServerInterface server;
    public static String guid;

    private static String adress = "diogoconstancio.com:10000";

    // THREAD
    private Thread work;

    private Thread checkConnection;
    private boolean answered;
    private int timer;

    // ----- METHODS -----

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Request user permissions
        ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_PERMISSIONS);

        // Set local variables
        connectButton = (Button) findViewById(R.id.connect);

        // Set buttons onclick events
        connectButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
            builder.setTitle("Server Address");

            // Set up the input
            final EditText input = new EditText(Menu.this);

            // Specify the type of input expected
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            // Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                work = new Thread(new Runnable() {
                    @Override
                    public void run() {
                    String tempAdress = input.getText().toString();

                    if (!tempAdress.equals(""))
                        adress = tempAdress;

                    if (isNetworkConnected()) {

                        try {
                            StringTokenizer st = new StringTokenizer(adress, ":");
                            String ip = st.nextToken();
                            int port = Integer.parseInt(st.nextToken());
                            answered = false;

                            // START CONNECTION THREAD CHECKER
                            checkConnection = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                timer = 0;
                                while(!answered && timer < 30){

                                    // Update UI seconds
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            connectButton.setText("Connect .. " + timer);
                                        }
                                    });

                                    // Sleep for 1 second
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }

                                    timer++;
                                }

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        connectButton.setText("Connect");
                                    }
                                });

                                if (!answered && work.isAlive()) {
                                    showMessage("Connection to server timed out");
                                    work.interrupt();
                                    work = null;
                                }
                                }
                            });
                            checkConnection.start();

                            // CONNECT TO SERVER
                            showMessage("Connecting to '" + adress + "'");

                            server = RpcClient.lookupService(ip, port, "Tracer", RMIServerInterface.class);
                            String hello = server.helloWorldRemote("Testing");
                            boolean connected = !hello.equals("");

                            // IF ALL OK
                            if (connected) {
                                showMessage("Connected to server successfully");

                                // UI THREAD
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                                    builder.setTitle("Username");

                                    // Set up the input
                                    final EditText input = new EditText(Menu.this);

                                    // Specify the type of input expected
                                    input.setInputType(InputType.TYPE_CLASS_TEXT);
                                    builder.setView(input);

                                    // Set up the buttons
                                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        work = new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                String tempID = input.getText().toString();

                                                if (!tempID.equals("")) {
                                                    guid = tempID;
                                                    server.connect(guid);

                                                    showMessage("Welcome " + guid);

                                                    Intent myIntent = new Intent(Menu.this, Tabs.class);
                                                    Menu.this.startActivity(myIntent);
                                                } else {
                                                    showMessage("Username can't be blank");
                                                }
                                            }
                                        });
                                        work.start();
                                        }
                                    });

                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.show();
                                    }
                                });
                            } else {
                                // ELSE NOT OK
                                showMessage("Error connecting to server");
                            }
                        } catch (NoSuchElementException n) {

                            showMessage("Invalid adress format, try 0.0.0.0:port");
                        } catch (NumberFormatException e) {
                            showMessage("Invalid port number");
                        } catch (Exception e) {
                            showMessage("Unable to connect to the server");
                        }

                        answered = true;

                    } else {
                        // UI THREAD
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                            builder.setMessage("Do you want to turn ON your WIFI?");

                            // Set up the buttons
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                changeWifiStatus(true);
                                }
                            });

                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                changeWifiStatus(false);
                                dialog.cancel();
                                }
                            });

                            builder.show();
                            }
                        });
                    }
                    }
                });

                work.start();
                }
            });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                }
            });

            builder.show();
            }
        });
    }

    // ----- AUXILIARY METHODS -----

    private void showMessage(String text) {
        // Get rootView
        View rootView = findViewById(android.R.id.content);

        Snackbar.make(rootView, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void changeWifiStatus(boolean a) {
        Context context = this.getApplicationContext();
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(a);
    }
}
