package Example;

import com.algoriteam.droidtracer_client.RMI.RpcServer;

public class ServerExample {
 
    public static void main(String[] args) {
        // create the RMI server
        RpcServer rpcServer = new RpcServer();
        // register a service under the name example
        // the service has to implement an interface for the magic to work
        rpcServer.registerService("example", new ServiceExample_Implementation());
        // start the server at port 6789
        rpcServer.start(6789);
    }

}
