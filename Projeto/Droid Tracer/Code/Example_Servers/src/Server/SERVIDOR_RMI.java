package Server;

import Main.PSST;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

public class SERVIDOR_RMI implements Runnable {

    //------------------------- VARIÁVEIS ------------------------------ 
    
    private int port;
    private String status;
    SERVIDOR_RMI_IMPL rmi_server;
    
    //------------------------- CONSTRUTOR ------------------------------ 
    
    public SERVIDOR_RMI() throws RemoteException, IOException {
        this.port = 0;
        this.status = "OFFLINE";
        this.rmi_server = new SERVIDOR_RMI_IMPL();
    }
    
    //------------------------- MÉTODOS ------------------------------ 
    
    // Define a "PORTA" na qual os ervidor irá ser iniciado
    public void setServer(int port) throws UnknownHostException, RemoteException{
        this.port = port;
    }
    
    // Desliga o servidor RMI
    public void shutdownServer() throws IOException{
        this.status = "OFFLINE";
        try{
            // Remover servico do registo
            Naming.unbind("heartbeat");
            // Remover objeto do "RMI Runtime"
            UnicastRemoteObject.unexportObject(rmi_server, true);
            PSST.addLog("[SHUTDOWN] Servidor RMI desligado");
        }
        catch(RemoteException | NotBoundException | MalformedURLException e){
        }
    }
    
    // Devolve o "ESTADO" do servidor (ONLINE ou OFFLINE)
    public String getStatus(){
        return this.status;
    }
    
    public void doCallbacks() throws RemoteException{
        this.rmi_server.doCallbacks();
    }
  
    /* Inicia o servidor na porta escolhida pelo administrador, vincula o novo servico 'heartbeat' ao 'rmiregistry' criado e ainda altera o "ESTADO" do servidor
       para ONLINE */
    @Override
    public void run() {
      try{   
        startRegistry(port);
        LocateRegistry.getRegistry().rebind("heartbeat", rmi_server);
        
        PSST.addLog("[ONLINE] Servidor RMI a correr");
        this.status = "ONLINE";
      }
      catch (RemoteException re) {
        System.out.println("[ERROR] Erro a iniciar o servico 'heartbeat'");
        PSST.addLog("[ERROR] Erro a iniciar o servico 'heartbeat'");
        this.status = "OFFLINE";
      }
    }

    // Inicia o 'rmiregistry' na máquina local
    private static void startRegistry(int RMIPortNum) throws RemoteException{
      try {
        LocateRegistry.createRegistry(RMIPortNum);
      }
      catch (RemoteException e) { 
      }
    }
}