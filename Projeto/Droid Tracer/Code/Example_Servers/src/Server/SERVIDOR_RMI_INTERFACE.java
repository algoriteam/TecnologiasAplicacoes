package Server;

import Client.CLIENTE_RMI_INTERFACE;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface SERVIDOR_RMI_INTERFACE extends Remote {
  
  /* Obtém a resposta do servidor HTTP utilizando o método PEDIDO que se baseia no CLIETE-SOCKET criado na FASE 1 do trabalho
     e mostra-o numa nova interfac criada para o cliente RMI*/
  public String listaPresencas(String ip_cliente) throws RemoteException;

  // Este método remoto permite a um objeto cliente registar-se na lsita de clientes que pretendem receber CallBack
  public void Registar_CallBack(CLIENTE_RMI_INTERFACE obj) throws RemoteException;

  // Este método remoto permite a um objeto cliente cancelar a sua "SUBSCRICAO" do CallBack
  public void Retirar_CallBack(CLIENTE_RMI_INTERFACE obj) throws RemoteException;
}