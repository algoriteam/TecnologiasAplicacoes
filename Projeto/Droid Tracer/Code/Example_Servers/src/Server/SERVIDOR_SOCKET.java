package Server;

import Main.PSST;
import Client.PEDIDO_SOCKET;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;

public class SERVIDOR_SOCKET extends Thread{ 
    
    //------------------------- VARIÁVEIS ------------------------------ 
    
    private int port;
    private String status;
    ServerSocket server;
    
    // Nº tentativas de ligação com ERRO ao servidor (NOTA: O limite é de 10, neste momento o servidor desliga e avisa no Output)
    private int tentativa;
 
    //------------------------- CONSTRUTOR ------------------------------ 
    
    public SERVIDOR_SOCKET(){
        this.port = 0;
        this.status = "OFFLINE";
        this.tentativa = 0;
    }
    
    //------------------------- MÉTODOS ------------------------------ 
    
    // Define a "PORTA" na qual os ervidor irá ser iniciado
    public void setServer(int port, ArrayList<String> Log_File){
        this.port = port;
    }
    
    // Desliga o servidor Socket
    public void shutdownServer() throws IOException{
        this.status = "OFFLINE";
        server.close();
        PSST.addLog("[SHUTDOWN] Servidor socket desligado");
    }
    
    // Devolve o "ESTADO" do servidor (ONLINE ou OFFLINE)
    public String getStatus(){
        return this.status;
    }
    
    // Inicia o servidor na porta escolhida pelo administrador e ainda altera o "ESTADO" do servidor para ONLINE
    @Override
    public void run(){ 
        // Inicia servidor-socket
        try{
            server = new ServerSocket(port);
            status = "ONLINE";
            PSST.addLog("[ONLINE] Servidor socket a correr");
            while (true){
                // Sempre que "alguém bate à porta" do servidor, este inicia uma nova thread para tentar responder ao pedido do cliente
                new PEDIDO_SOCKET(server.accept(), tentativa).start();
            }
        }catch (IOException e) {
            status = "OFFLINE";
            if (!server.isClosed()){
                PSST.addLog("[ERROR] Erro a escutar a porta "+port+"");
                System.out.println("[ERROR] Erro a escutar a porta "+port+"");
            }
          }
    }
}
