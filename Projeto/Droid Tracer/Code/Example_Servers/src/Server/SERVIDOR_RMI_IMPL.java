package Server;

import Main.PSST;
import Client.CLIENTE_RMI_INTERFACE;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class SERVIDOR_RMI_IMPL extends UnicastRemoteObject implements SERVIDOR_RMI_INTERFACE {

    //------------------------- VARIÁVEIS ------------------------------ 
    
    // Lista de clientes que se encontram registados para CallBack
    private static ArrayList<CLIENTE_RMI_INTERFACE> clientList;
    
    // Nº tentativas de ligação com ERRO ao servidor (NOTA: O limite é de 10, neste momento o servidor desliga e avisa no Output)
    private int tentativa;
    
    //------------------------- CONSTRUTOR ------------------------------ 
    
    public SERVIDOR_RMI_IMPL() throws IOException {
        super();
        this.clientList = new ArrayList();
        this.tentativa = 0;
    }

    //------------------------- MÉTODOS ------------------------------ 
    
    /* Obtém a resposta do servidor HTTP utilizando o método PEDIDO que se baseia no CLIETE-SOCKET criado na FASE 1 do trabalho
       e mostra-o numa nova interfac criada para o cliente RMI*/
    @Override
    public String listaPresencas(String ip_cliente) throws RemoteException {
        String pedido="";
        StringBuilder resposta = new StringBuilder();
        
        resposta.append("[LIGACAO ESTABELECIDA] A obter informação: \n");
        try {
            // Obtém a resposta do servidor HTTP utilizando o método PEDIDO que se baseia no CLIETE-SOCKET criado na FASE 1 do trabalho
            pedido = PSST.PEDIDO(ip_cliente);
            if(pedido.equals("ERROR")){
                tentativa++;
                resposta.append("[CONNECTION-ERROR] Servidor com problemas, por favor tente mais tarde\n");
                PSST.addLog("[HTTP-SERVER-ERROR] Erro de ligacao ao servidor 'heartbeat.dsi.uminho.pt' de "+ip_cliente+"\n    -> Nºtentativas registadas: "+tentativa+"");
                // Após um número limite de tentativas de ligação ao servidor do DSI, o SERVIDOR-SOCKET desliga-se e mostra um erro
                if(tentativa>=10){
                    PSST.addLog("[ATTEMPT-LIMIT-SHUTDOWN] A DESLIGAR o servidor para manutencao, por favor verifique a sua LIGACAO com o hostname ou PORTA utilizada");
                    System.exit(0);
                }
            }
            else{
                // Responde ao cliente com a lista dos últimos ip's que realizaram ligação ao servidor HTTP tal como pedido para este projeto
                resposta.append(pedido);
                // Mostra no ouput do programa do SERVIDOR uma nova mensagem de ligação
                PSST.addLog("[CONNECTION] Lista de IP's enviados para "+ip_cliente+"");
                // Atualiza para a versão mais recente o backup da "LISTA" de IP's que se encontra no sistema
                PSST.updateLista(resposta.toString());
            }
        } catch (IOException ex) { 
             PSST.addLog("[HTTP-SERVER-ERROR] Erro de ligacao ao servidor 'heartbeat.dsi.uminho.pt' de "+ip_cliente+"\n    -> Nºtentativas registadas: "+tentativa+"");
        }
        return resposta.toString();
    }

    // Este método remoto permite a um objeto cliente registar-se na lsita de clientes que pretendem receber CallBack
    @Override
    public void Registar_CallBack(CLIENTE_RMI_INTERFACE obj) throws RemoteException{
        // Guarda o objeto do cliente que realizou o pedido de "CALLBACK" num ARRAY
           clientList.add(obj);
           PSST.addLog("[CALLBACK] Novo cliente registado ["+obj.getIp()+"]");
           doCallbacks();
    }  

    // Este método remoto permite a um objeto cliente cancelar a sua "SUBSCRICAO" do CallBack
    @Override
    public void Retirar_CallBack(CLIENTE_RMI_INTERFACE obj) throws RemoteException{
         if (clientList.remove(obj)) PSST.addLog("[CALLBACK] Cliente removido ["+obj.getIp()+"]");
    }

    public void doCallbacks( ) throws RemoteException{
        // Notifica todos os utilizadores que se encontram registados para receber CallBack
        PSST.addLog("[CALLBACK] A notificar clientes [INICIAR]");
        for (int i = 0; i < clientList.size(); i++){
            PSST.addLog("[CALLBACK] A realizar o "+(i+1)+"º CallBack");   
            // Converte o objeto do tipo ARRAY para um objeto do tipo CallBack
            CLIENTE_RMI_INTERFACE nextClient = (CLIENTE_RMI_INTERFACE) clientList.get(i);
            // Invoca o método que inciia o CallBack
            nextClient.notifyMe("\n"+PSST.getLista());
        }
            PSST.addLog("[CALLBACK] Notificação completa [FINALIZAR]");
    }
}