package Client;

import Main.PSST;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class PEDIDO_SOCKET extends Thread {
    
    private Socket socket;
    private int tentativa;
    
    public PEDIDO_SOCKET(Socket soc, int tenta){
        this.socket = soc;
        this.tentativa = tenta;
    }
    @Override
    public void run(){
        String ip_cliente = "";
        String pedido = "";
        String resposta;
        try (
                // Inicia ligação com o cliente
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ){     
                //Obtém o ip do cliente que realizou a ligação ao servidor
                ip_cliente = socket.getInetAddress().getHostAddress();
                out.println("[LIGACAO ESTABELECIDA] A obter informação: \n");
                //Resposta do servidor
                pedido=in.readLine();
                    if (pedido.equals("LISTA IP")){
                            // Obtém a resposta do servidor HTTP utilizando o método PEDIDO que se baseia no CLIETE-SOCKET criado na FASE 1 do trabalho
                            resposta = PSST.PEDIDO(ip_cliente); 
                        if(resposta.equals("ERROR")){
                            tentativa++;
                            out.println("[CONNECTION-ERROR] Servidor com problemas, por favor tente mais tarde");
                            PSST.addLog("[HTTP-SERVER-ERROR] Erro de ligacao ao servidor 'heartbeat.dsi.uminho.pt' de "+ip_cliente+"\n    -> Nºtentativas registadas: "+tentativa+"");
                            // Após um número limite de tentativas de ligação ao servidor do DSI, o SERVIDOR-SOCKET desliga-se e mostra um erro
                            if(tentativa>=10){
                                PSST.addLog("[ATTEMPT-LIMIT-SHUTDOWN] A DESLIGAR o servidor para manutencao, por favor verifique a sua LIGACAO com o hostname ou PORTA utilizada");
                                //Fechar STREAMS
                                out.close();
                                in.close();
                                // Termina ligação com o cliente - IMPORTANTE
                                socket.close();
                                System.exit(0);
                            }
                        }
                        else{
                        // Responde ao cliente com a lista dos últimos ip's que realizaram ligação ao servidor HTTP tal como pedido para este projeto
                            out.println(resposta);
                            // Mostra no ouput do programa do SERVIDOR uma nova mensagem de ligação
                            PSST.addLog("[CONNECTION] Lista de IP's enviados para "+ip_cliente+"");
                            // Atualiza para a versão mais recente o backup da "LISTA" de IP's que se encontra no sistema
                            PSST.updateLista(resposta);
                        }
                    }
                    else {
                        out.println("[PROTOCOL-ERROR] Nao conseguimos obter informacao com o seu pedido\n    -> PROTOCOLOS:\n                   [1] 'LISTA IP' - Lista os IP's das ultimas maquinas que realizaram uma ligacao ao servidor");
                        // Mostra no ouput do programa do SERVIDOR uma nova mensagem de ligação
                        PSST.addLog("[CONNECTION] Pedido invalido de "+ip_cliente+"");
                    }
                //Fechar STREAMS
                out.close();
                in.close();
                // Termina ligação com o cliente - IMPORTANTE
                socket.close();
        }catch (IOException e){  
        }
    }
}