package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class CLIENTE_SOCKET {
        public static void main(String[] args) throws IOException  {
            /*INFO - http://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
              Um SOCKET consiste numa 'ponte de ligação' entre dois programas na mesma rede (Neste caso Cliente <-> Servidor)*/
            
            // Hostname do computador pessoal pode ser obtido pela consola utilizando o comando "ipconfig /all"
            String hostname = "localhost";
            String resposta;
            
            //Port "80" escolhido porque 'https://pt.wikipedia.org/wiki/Hypertext_Transfer_Protocol#GET'
            try{
                Socket socketclient = new Socket(hostname, 4444);
                PrintWriter out = new PrintWriter(socketclient.getOutputStream());
                BufferedReader in = new BufferedReader(new InputStreamReader(socketclient.getInputStream()));
            //Pedido de acordo com protocolo do NOVO SERVIDOR
                out.println("LISTA IP");
            /*Utilização do flush justifica-se pela necessidade de a mensagem precisar de 'aparecer' logo no servidor após escrita caso contrário fica
            em espera!*/
                out.flush();
            //Resposta do servidor
            while((resposta=in.readLine())!=null){
                System.out.println(resposta);
            }
            
            //INFO - http://docs.oracle.com/javase/tutorial/networking/sockets/readingWriting.html
            
            //Fechar STREAMS
                out.close();
                in.close();
            
            //Fechar SOCKET
            socketclient.close();
            }catch(IOException e){
                System.out.println("[ERROR] Problema de ligacao ao servidor, verifique a PORTA registada ou se o servidor se encontra ONLINE");
            }
        }
}