package Client;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CLIENTE_RMI_IMPL extends UnicastRemoteObject implements CLIENTE_RMI_INTERFACE {
  
   //------------------------- CONSTRUTOR ------------------------------
    
   public CLIENTE_RMI_IMPL() throws RemoteException {
      super();
   }

   //------------------------- MÉTODOS ------------------------------
   
   // Este método remoto é invocado pelo servidor que realiza o "CALLBACK" para um utilizador que implemente a interface CLIENTE_RMI_INTERFACE
   @Override
   public String notifyMe(String message){
      DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd [HH:mm:ss]");
      Calendar cal = Calendar.getInstance();
      
      clearConsole();
      System.out.println("------------------------------------------------");
      System.out.println("LAST UPDATE:       "+ dateFormat.format(cal.getTime())); //2014/08/06 16:00:22
      System.out.print("------------------------------------------------");
      System.out.println(message);
      System.out.println("[MESSAGE] CallBack recebido");
      return message;
   }
   
   
   // Este método remoto é invocado pelo servidor para obter o IP do utilizador que implemente a interface CLIENTE_RMI_INTERFACE
   @Override
   public String getIp() {
       try {
           return InetAddress.getLocalHost().getHostAddress();
       } catch (UnknownHostException ex) {
       }
       return "IP.ERROR";
   }  
   
   //------------------------- MÉTODOS EXTRA ------------------------------
  
   // Limpa o ecrâ da consola
   public static void clearConsole(){
        for(int i = 0; i < 15; i++) System.out.println(); 
   }

}