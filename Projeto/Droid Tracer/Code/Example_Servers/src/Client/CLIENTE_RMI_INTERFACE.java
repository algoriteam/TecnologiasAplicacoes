package Client;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CLIENTE_RMI_INTERFACE extends Remote{

    //------------------------- MÉTODOS ------------------------------
    
    // Este método remoto é invocado pelo servidor que realiza o "CALLBACK" para um utilizador que implemente esta interface
    public String notifyMe(String message) throws RemoteException;
    
    // Este método remoto é invocado pelo servidor para obter o IP do utilizador que implemente esta interface
    public String getIp() throws RemoteException;

}