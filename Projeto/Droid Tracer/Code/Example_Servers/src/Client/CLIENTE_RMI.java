package Client;

import Server.SERVIDOR_RMI_INTERFACE;
import java.rmi.RemoteException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.util.Scanner;

public class CLIENTE_RMI {
  
   private static CLIENTE_RMI_IMPL cli; 
    
  //------------------------- MAIN ------------------------------    
  
  public static void main(String args[]) {
    Menu();
  }
 
  //------------------------- MÉTODOS ------------------------------  
  
  // Menu principal da plataforma do cliente RMI
  private static void Menu(){
        Scanner ler = new Scanner(System.in);
        String ESTADO = "";
        char option;
        clearConsole();
        System.out.println("------------------------------------------------\n   ____              _             _");
        System.out.println("  |  _ \\ ___ ___ ___| |_          | |");
        System.out.println("  | |_) / __/ __/ __| __|         | |");
        System.out.println("  |  __/\\__ \\__ \\__ | |_   _ _ _  |_|");
        System.out.println("  |_|   |___|___|___/\\__| (_(_(_) (_) [CLIENT]\n");
        System.out.println("          1 - Lista Presenças\n          2 - CallBack\n");
        System.out.println("------------------------------------------------\n                                     Q - Sair\n------------------------------------------------");
        System.out.print("-> Insira opção: ");
        option = ler.nextLine().charAt(0);
        switch(Character.toUpperCase(option)){
            case '1': ESTADO = "LISTA";
                      break;
            case '2': ESTADO = "CALL";
                      break;
            case 'Q': System.exit(0);
                
            default: Menu();
                     break;
        }
        if (ESTADO.equals("LISTA")) Lista_Presencas();
        if (ESTADO.equals("CALL")) Registar_Callback();
    }
  
  // Obtém a "LISTA" de IP's por recurso a métodos do serviço 'heartbeat' que se encontra vinculado no 'rmiregistry' criado pelo servidor RMI
  private static void Lista_Presencas(){
      try {        
      String hostName = "localhost";
      String servico =  "heartbeat";
      String myip = InetAddress.getLocalHost().getHostAddress();
      
      Scanner ler = new Scanner(System.in);
      char leitura;
      String ESTADO = "";
      
      // Encontra o objeto remoto e realiza cast dele para uma interface objeto
    
      SERVIDOR_RMI_INTERFACE h = (SERVIDOR_RMI_INTERFACE) LocateRegistry.getRegistry(hostName).lookup(servico);
      while (ESTADO.equals("")){
          clearConsole();
          System.out.println("------------------------------------------------");
          System.out.println(h.listaPresencas(myip));
          System.out.println("------------------------------------------------");
          System.out.println("   R - Retroceder");
          System.out.println("------------------------------------------------");
          System.out.print("-> Insira opção : ");
          leitura = ler.next().charAt(0);
          switch (Character.toUpperCase(leitura)) {
              case 'R':  ESTADO = "MENU";
                         break;
              default :  ESTADO = "";
                         break;
          }   
      }
      if (ESTADO.equals("MENU")) Menu();
    }
    catch (NumberFormatException | NotBoundException | RemoteException | UnknownHostException e) {
        System.out.println("[ERROR] Problema de ligacao ao servidor");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex){
        }
        Menu();
    }
  }
  
  /* Adiciona o cliente a uma lista de "CLIENTES ESPECIAIS" que pretendem manter - se atualizados sempre que houver uma alteração
     na "LISTA" de IP's devido a uma ligação de um outro utilizador */
  private static void Registar_Callback(){
      try {         
      String hostName = "lenovo";
      String servico =  "heartbeat";
      
      Scanner ler = new Scanner(System.in);
      char leitura;
      String ESTADO = "";
      
      System.out.print("Quanto tempo pretende ficar ligado? (em segundos):");
      String timeDuration = ler.nextLine();
      int time = Integer.parseInt(timeDuration);
      
      // Encontra o objeto remoto e realiza cast dele para uma interface objeto
      
      SERVIDOR_RMI_INTERFACE h = (SERVIDOR_RMI_INTERFACE) LocateRegistry.getRegistry(hostName).lookup(servico);
      while (ESTADO.equals("")){
          cli = new CLIENTE_RMI_IMPL();
          System.out.println("[MESSAGE] Registado para CallBack");
          try {
            Thread.sleep(2000);
          } catch (InterruptedException ex){
          }
          h.Registar_CallBack(cli);
          try {
            Thread.sleep((time+2) * 1000);
          } catch (InterruptedException ex){
          }
          h.Retirar_CallBack(cli);
          System.out.println("[MESSAGE] Retirado do CallBack");
          System.out.println("------------------------------------------------");
          System.out.println("   R - Retroceder");
          System.out.println("------------------------------------------------");
          System.out.print("-> Insira opção : ");
          leitura = ler.next().charAt(0);
          switch (Character.toUpperCase(leitura)) {
              case 'R':  ESTADO = "MENU";
                         break;
              default :  ESTADO = "";
                         break;
          }   
      }
      if (ESTADO.equals("MENU")) Menu();
    }
    catch (NumberFormatException | NotBoundException | RemoteException e) {
      System.out.println("[ERROR] Ocorreu um erro por parte do servidor");
      try {
            Thread.sleep(2000);
      } catch (InterruptedException ex){
      }
      Menu();
    }
  }
  
  //------------------------- MÉTODOS EXTRA ------------------------------
  
  // Limpa o ecrâ da consola
  public static void clearConsole(){
            for(int i = 0; i < 15; i++) System.out.println(); 
    }
}