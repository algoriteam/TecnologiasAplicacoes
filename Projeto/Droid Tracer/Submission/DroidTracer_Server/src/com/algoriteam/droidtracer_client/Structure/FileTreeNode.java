package com.algoriteam.droidtracer_client.Structure;

import java.io.Serializable;
import java.util.ArrayList;

public class FileTreeNode implements Serializable {
    
    // ----- VARIABLES -----

    private String name;
    private ArrayList<FileTreeNode> nodes;

    // ----- CONSTRUCTOR -----

    public FileTreeNode(String n) {
        name = n;
        nodes = new ArrayList<>();
    }

    // ----- GETTERS -----

    public String getName() {
        return name;
    }

    public ArrayList<FileTreeNode> getNodes() {
        return nodes;
    }

    // ----- SETTERS -----

    public void setName(String name) {
        this.name = name;
    }

    public void setNodes(ArrayList<FileTreeNode> nodes) {
        this.nodes = nodes;
    }

    // ----- OTHER METHODS -----

    public void addFileTreeNode(FileTreeNode a) {
        nodes.add(a);
    }

    public boolean isFile() {
        return nodes.size() == 0;
    }

    public boolean isFolder() {
        return nodes.size() > 0;
    }

}