package com.algoriteam.droidtracer_client.Client;

import com.algoriteam.droidtracer_client.Structure.FileTreeNode;
import com.algoriteam.droidtracer_client.Server.RMIServerInterface;
import com.algoriteam.droidtracer_client.RMI.RpcClient;
import java.io.IOException;

public class RMIClient {

    private static final String exampleFile = "\n" +
            "LookAt 0 -3.4 0   0 1 0 0 0 1\n" +
            "Camera \"perspective\" \"float fov\" [45]\n" +
            "Sampler \"stratified\" \"integer xsamples\" [4] \"integer ysamples\" [4]\n" +
            "Film \"image\" \"string filename\" [\"dist-1min.exr\"] \"integer xresolution\" [64] \"integer yresolution\" [64]\n" +
            "\n" +
            "WorldBegin\n" +
            "\n" +
            "Identity\n" +
            "\n" +
            "#Area Light Source\n" +
            "AttributeBegin\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  # 1st row\n" +
            "  Translate .95 -.95 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 2nd row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 -.75 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 3rd row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 -.55 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 4th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 -.35 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 5th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 -.15 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 6th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 .15 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 7th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 .35 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 8th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 .55 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 9th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 .75 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 10th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 .95 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 11th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 1.15 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 12th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 1.35 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 13th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 1.55 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 14th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .85 1.75 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  # 15th row\n" +
            "  Identity\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate .95 1.95 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "  AreaLightSource \"area\" \n" +
            "\t\t\"color L\"    [10 10 10]\n" +
            "\t\t\"integer nsamples\" [1]\n" +
            "  Translate -.2 0 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "\t\t\"integer indices\" [2 1 0 2 3 1]\n" +
            "\t\t\"point P\" [-0.03 -0.03 0.99     0.03 -0.03 0.99     -0.03 0.03 0.99     0.03 0.03 0.99]\n" +
            "AttributeEnd\n" +
            "\n" +
            "# walls\n" +
            "AttributeBegin\n" +
            "  # white walls  material\n" +
            "  Material \"plastic\" \n" +
            "              \"color Kd\"    [1 1 1]\n" +
            "\t\t\"color Ks\"    [0.1 0.1 0.1]\n" +
            "\t\t\"float roughness\" 0.15\n" +
            "  # back wall\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-2 2 -1   -2 2 1   1 2 1   1 2 -1]\n" +
            " # floor\n" +
            " Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-2 2 -1   1 2 -1   1 -1 -1   -2 -1 -1] \n" +
            "  # left distant wall\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-2 1 -1   -2 1 1   -2 2 1   -2 2 -1]\n" +
            "  # distant front wall\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-1 1 -1   -1 1 1   -2 1 1   -2 1 -1]\n" +
            "\n" +
            " \n" +
            " # aluminium ceiling material\n" +
            "  Material \"metal\"  \"float roughness\" [.03]\n" +
            "      \"spectrum eta\" \"spds/metals/Al.eta.spd\"\n" +
            "      \"spectrum k\" \"spds/metals/Al.k.spd\"\n" +
            "\n" +
            "  # ceiling\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [2 1 0 0 3 2]\n" +
            "\t\t\"point P\" [-2 2 1   1 2 1   1 -1 1   -2 -1 1]\n" +
            "\t\t\"float uv\" [1 0 0 0 0 1 1 1]\n" +
            "  # red wall material\n" +
            "  Material \"plastic\" \n" +
            "              \"color Kd\"    [0.8 0.1 0.1]\n" +
            "\t\t  \"color Ks\"    [0.1 0.1 0.1]\n" +
            "\t\t  \"float roughness\" 0.15\n" +
            "  # left red wall\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-1 -1 -1   -1 -1 1   -1 1 1   -1 1 -1]\n" +
            "  # blue wall material\n" +
            "  Material \"plastic\" \n" +
            "              \"color Kd\"    [0.2 0.3 0.8]\n" +
            "\t\t  \"color Ks\"    [0.1 0.1 0.1]\n" +
            "\t\t  \"float roughness\" 0.15\n" +
            "  # right blue wall\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [2 1 0 0 3 2]\n" +
            "\t\t\"point P\" [1 -1 -1   1 -1 1   1 2 1   1 2 -1]\n" +
            "AttributeEnd\n" +
            "\n" +
            "# mirror\n" +
            "AttributeBegin\n" +
            "  # mirror material\n" +
            "  Material \"mirror\" \n" +
            "\t\t  \"color Kr\"    [0.9 0.9 0.9]\n" +
            "  # mirror\n" +
            "  Translate 0 1 0\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [2 1 0 1 3 2]\n" +
            "\t\t\"point P\" [0.99 -0.65 0    0.99 0.65 0   0.99 -0.65 -0.9        0.99 0.65 -0.9]\n" +
            "AttributeEnd\n" +
            "\n" +
            "# green prism\n" +
            "AttributeBegin\n" +
            "  Material \"plastic\" \n" +
            "           \"color Kd\"    [0.1 0.9 0.1]\n" +
            "\t\t\"color Ks\"    [0 0 0]\n" +
            "\t\t\"float roughness\" 0.15\n" +
            "  Translate 0.6 1.4 -.7\n" +
            "  Rotate 45 0 0 -1\n" +
            "  Scale 0.25 0.25 0.3\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0 0 1 5 5 4 0 1 2 5 5 6 2 2 6 3 3 6 7 0 3 4 4 7 3 7 4 5 5 6 7 ]\n" +
            "\t\t\"point P\" [-1 1 -1   -1 1 1   1 1 1   1 1 -1   -1 -1 -1   -1 -1 1   1 -1 1   1 -1 -1]\n" +
            "AttributeEnd\n" +
            "\n" +
            "#teapot\n" +
            "AttributeBegin\n" +
            "  Material \"metal\"  \"float roughness\" [.001]\n" +
            "      \"spectrum eta\" \"spds/metals/Au.eta.spd\"\n" +
            "      \"spectrum k\" \"spds/metals/Au.k.spd\"\n" +
            "\n" +
            "  Translate 0.65 1.4 -.41\n" +
            "  Rotate 090 1 0 0\n" +
            "  Rotate 45 0 -1 0\n" +
            "  Scale 0.15 0.15 0.15\n" +
            "  Include \"geometry/room-teapot.pbrt\"\n" +
            "AttributeEnd\n" +
            "\n" +
            "#killeroo\n" +
            "AttributeBegin\n" +
            "  Material \"glass\"  \n" +
            "      \"float index\" [2.0]\n" +
            "      \"spectrum Kt\" [300 0.9 400 0.9 500 0.9 600 0.9 700 0.9 800 0.9]\n" +
            "      \"spectrum Kr\" [300 0.1 400 0.1 500 0.1 600 0.1 700 0.1 800 0.1]\n" +
            "\n" +
            "  Translate -0.45 -.35 -.45\n" +
            "  Rotate 135 0 0 1\n" +
            "  Scale 0.004 0.004 0.004\n" +
            "  Include \"geometry/killeroo.pbrt\"\n" +
            "AttributeEnd\n" +
            "\n" +
            "# painting\n" +
            "AttributeBegin\n" +
            "  Texture \"painting\" \"color\" \"imagemap\" \"string filename\" \"textures/oGrito_EdwardMunch.tga\" \"float scale\" [2] \"float gamma\" [2.]\n" +
            "  Material \"plastic\" \n" +
            "              \"texture Kd\"    \"painting\"\n" +
            "\t\t  \"color Ks\"    [0.0 0.0 0.0]\n" +
            "  # canvas\n" +
            "  Scale 1.2 1 1.2\n" +
            "  Shape \"trianglemesh\" \n" +
            "     \t\t\"integer indices\" [0 1 2 2 3 0]\n" +
            "\t\t\"point P\" [-.6 1.99 -.3   -.6 1.99 .44   0 1.99 .44   0 1.99 -.3]\n" +
            "           \"float uv\" [1 0 1 1 0 1 0 0]\n" +
            "\n" +
            "AttributeEnd\n" +
            "\n" +
            "\n" +
            "\n" +
            "WorldEnd\n";

    public static void main(String[] args) throws IOException, InterruptedException {
        // Lookup the service with name "Tracer", interface RMIServerInterface located at host localhost and port 10000
        RMIServerInterface server = RpcClient.lookupService("localhost", 10000, "Tracer", RMIServerInterface.class);

        String w = server.helloWorldRemote("test");
        server.connect("asddsasad");
        FileTreeNode ft = server.listResources("asddsasad");
        
        server.startRender(exampleFile, "asddsasad");
        server.getStatus("asddsasad");
        server.getRenderedImage("asddsasad");
    }
}
