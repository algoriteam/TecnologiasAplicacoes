package com.algoriteam.droidtracer_client.Server;

import com.algoriteam.droidtracer_client.Structure.FileTreeNode;
import com.algoriteam.droidtracer_client.Structure.Pair;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

public class RMIServerImpl implements RMIServerInterface {

    // ----- VARIABLES -----
        
        private Thread work;
        private HashMap<String, Integer> clients;
        private HashMap<String, Process> clients_process;
    
    // ----- CONSTRUCTOR -----
        
    public RMIServerImpl() {
        clients = new HashMap<>();
        clients_process = new HashMap<>();
    }
    
    // ----- METHODS -----
            
    @Override
    public String helloWorldRemote(String test) {
        System.out.println("[DROID TRACER SERVER] Someone is reaching me");
        return "Hello from server '" + test + "'";
    }
    
    @Override
    public boolean connect(String guid) {
        System.out.println("[DROID TRACER SERVER] connect - " + guid);
        clients.putIfAbsent(guid, 0);
        return true;
    }
    
    @Override
    public boolean disconnect(String guid) {
        System.out.println("[DROID TRACER SERVER] disconnect - " + guid);
        return true;
    }

    @Override
    public void startRender(String guid, String fileContents) throws IOException {
        System.out.println("[DROID TRACER SERVER] startRender - " + guid );
        
        work = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    File  pbrtInput = new File("IN_" + guid + ".pbrt");
                    File  pbrtOutput = new File("OUT_" + guid + ".tracer");
                    File  pbrtError = new File("ERROR_" + guid + ".tracer");
                    File  exr = new File("IMAGE_" + guid + ".exr");
                    
                    clients.put(guid, 1);

                    // Replace output filename .exr in fileContents
                    String newFileContents = fileContents.replaceAll("\"[a-zA-Z_\\-\\.0-9]*.exr\"", "\"" + "IMAGE_" + guid + ".exr\"");

                    try (PrintWriter writer = new PrintWriter(pbrtInput)) {
                        writer.write(newFileContents);
                        writer.flush();
                    }

                    ProcessBuilder builder = new ProcessBuilder("bash");
                    builder.redirectError(pbrtError);
                    builder.redirectOutput(pbrtOutput);

                    Process process = builder.start();
                    clients_process.put(guid, process);

                    // Get stdin of shell
                    try (BufferedWriter p_stdin = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()))) {
                        p_stdin.write("./pbrt " + "IN_" + guid + ".pbrt\n");
                        p_stdin.flush();
                    }

                    process.waitFor();
                    
                    // CHECK IF OUTPUT FINISHED
                    Pair temp = getTimeRemaining(guid);
                    if (exr.exists() && temp.getFirst() == -1.0 && temp.getSecond() == -1.0) {
                        // DONE
                        clients.put(guid, 2);
                    } else {
                        if (pbrtInput.exists() && pbrtOutput.exists() && pbrtError.exists()) {
                            if (temp != null && temp.getFirst() != -1.0 && temp.getSecond() != -1.0) {
                                clients.put(guid, 1);
                            } else {
                                // FOUND ERRORS
                                clients.put(guid, 3);
                            }
                        } else {
                            clients.put(guid, 0);
                        }
                    }
                    
                    clients_process.remove(guid);

                }
                catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        work.start();
    }
    
    @Override
    public void stopRendering(String guid) {
        System.out.println("[DROID TRACER SERVER] stopRendering - " + guid );

        clients_process.get(guid).destroyForcibly();
        deleteTempFiles(guid);
    }

    @Override
    public Pair getTimeRemaining(String guid)
    {
        File  pbrtOutput = new File("OUT_" + guid + ".tracer");
        
        if(pbrtOutput.exists()) {
            String tail = tail(pbrtOutput);
            String[] slices = tail.split("\\(|s\\)|s\\|");

            if(slices.length == 4){
                return new Pair(Double.valueOf(slices[1]), Double.valueOf(slices[2]));
            } else
                return new Pair(-1.0, -1.0);
        } else {
            return null;
        }
    }

    @Override
    public byte[] getRenderedImage(String guid) throws IOException {
        File  exr = new File("IMAGE_" + guid + ".exr");
        File  image = new File("IMAGE_" + guid + ".jpg");
        
        System.out.println("[DROID TRACER SERVER] getRenderedImage - " + guid );
        byte[] toRet = null;
        
        try {
            if(exr.exists()) {

                ProcessBuilder builder = new ProcessBuilder("bash");
                Process p = null;
                try {
                    p = builder.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // Get stdin from shell
                BufferedWriter p_stdin = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

                p_stdin.write("pfsin " + "IMAGE_" + guid + ".exr | pfsout " + "IMAGE_" + guid + ".jpg" );
                p_stdin.flush();
                p_stdin.close();

                p.waitFor();

                Path path = Paths.get(image.getAbsolutePath());
                toRet = Files.readAllBytes(path);
                
                deleteTempFiles(guid);
                clients.put(guid, 0);
            }
        } catch(InterruptedException e) {
            e.printStackTrace();
        }

        return toRet;
    }

    @Override
    public int getStatus(String guid)
    {
        // 0 - waiting
        // 1 - rendering
        // 2 - finished
        // 3 - an error occurred
        return clients.get(guid);
    }
    @Override
    public FileTreeNode listResources(String guid)
    {
        System.out.println("[DROID TRACER SERVER] listResources - " + guid );
        
        File root = new File("./Resources");
        FileTreeNode rootNode = new FileTreeNode("Resources");
        File[] files = root.listFiles();

        for(File file:files)
            listResourcesAux(rootNode, file);

        return rootNode;
    }

    // ----- AUXILIARY METHODS -----
    
    private void deleteTempFiles(String guid) {
        File  pbrtInput = new File("IN_" + guid + ".pbrt");
        File  pbrtOutput = new File("OUT_" + guid + ".tracer");
        File  pbrtError = new File("ERROR_" + guid + ".tracer");
        File  exr = new File("IMAGE_" + guid + ".exr");
        File  image = new File("IMAGE_" + guid + ".jpg");
        
        pbrtInput.delete();
        pbrtOutput.delete();
        pbrtError.delete();
        exr.delete();
        image.delete();
    }
        
    private void listResourcesAux(FileTreeNode parentNode, File me)
    {
        FileTreeNode thisNode = new FileTreeNode(me.getName());

        if(me.isDirectory()) {
            File[] files = me.listFiles();

            for (File file : files)
                listResourcesAux(thisNode, file);

        }
        parentNode.getNodes().add(thisNode);
    }


    private String tail(File file) {
        RandomAccessFile fileHandler = null;
        try {
            fileHandler = new RandomAccessFile(file, "r");
            long fileLength = fileHandler.length() - 1;
            StringBuilder sb = new StringBuilder();

            for(long filePointer = fileLength; filePointer != -1; filePointer--){
                fileHandler.seek(filePointer);
                int readByte = fileHandler.readByte();

                if(readByte == 0xA) {
                    if(filePointer == fileLength) {
                        continue;
                    }
                    break;

                } else if(readByte == 0xD) {
                    if(filePointer == fileLength - 1) {
                        continue;
                    }
                    break;
                }

                sb.append((char) readByte);
            }

            String lastLine = sb.reverse().toString();
            return lastLine;
        } catch( java.io.FileNotFoundException e ) {
            e.printStackTrace();
            return null;
        } catch( java.io.IOException e ) {
            e.printStackTrace();
            return null;
        } finally {
            if (fileHandler != null )
                try {
                    fileHandler.close();
                } catch (IOException e) {
                    /* ignore */
                }
        }
    }

}
