package Example;

// very straight forward
public class ServiceExample_Implementation implements ServiceExample_Interface {

	@Override
	public String concat(String... args) {
		if (args == null || args.length == 0) {
			return "";
		}
		String concat = "";
		for (String arg : args) {
			concat += arg;
		}
		return concat;
	}

 
}
