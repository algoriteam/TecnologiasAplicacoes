package Example;

import com.algoriteam.droidtracer_client.RMI.RpcClient;

public class ClientExample {
 
    public static void main(String[] args) {
        // lookup the service with name "example", interface ServiceExample located at host localhost and port 6789
        ServiceExample_Interface example = RpcClient.lookupService("localhost", 6789, "example", ServiceExample_Interface.class);
        // call the method concat and display the result
        System.out.println(example.concat("foo", " ", "bar", " ", "baz"));
    }

}
