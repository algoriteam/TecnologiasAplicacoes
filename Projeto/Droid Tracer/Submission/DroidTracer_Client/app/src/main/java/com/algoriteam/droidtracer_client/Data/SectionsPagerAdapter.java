package com.algoriteam.droidtracer_client.Data;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.algoriteam.droidtracer_client.Presentation.Tabs;

import java.util.HashMap;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    // ----- VARIABLES -----

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    // ----- METHODS -----

    @Override
    public Fragment getItem(int position) {
        return PlaceholderFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Resources";
            case 1:
                return "Render";
            case 2:
                return "Results";
        }
        return null;
    }

}
