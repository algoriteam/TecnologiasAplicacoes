package com.algoriteam.droidtracer_client.Data;

import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.algoriteam.droidtracer_client.Presentation.Menu;
import com.algoriteam.droidtracer_client.Presentation.Tabs;
import com.algoriteam.droidtracer_client.R;
import com.algoriteam.droidtracer_client.Structure.FileTreeNode;
import com.algoriteam.droidtracer_client.Structure.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

import static android.app.Activity.RESULT_OK;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    // ----- VARIABLES -----

    // The fragment argument representing the section number for this fragment.
    private static final String ARG_SECTION_NUMBER = "section_number";

    // Activities
    private static final int SHOW_FILE_CHOOSER = 0;

        // VIEW
        private View rootView;

        // PAGE 1
        private FloatingActionButton start;
        private FloatingActionButton delete;
        private ImageView search;
        private ImageView searchBack;
        private TextView filename;
        private ProgressBar progress;
        private TextView percentage;
        private TextView logger;

        private ImageView finalImage;

        private int status;
        private Pair timings;
        private double time;

        private File chosenFile;
        private String chosenPath;
        private String chosenName;
        private String chosenExtension;
        private String filecontents;
        private byte[] result;
        private Bitmap chosenBitmap;

        // PAGE 2
        private FloatingActionButton support;
        private GridView grid;

        private ArrayList<Integer> path;
        private TextView foldername;
        private FileTreeNode root;

        // PAGE 3
        private FloatingActionButton local_support;
        private GridView local_grid;
        private ArrayList<String> local_root;

    // THREAD
    private Thread work;
    private boolean keepRendering;

    // ----- CONSTRUCTOR -----

    public PlaceholderFragment() {}

    // Returns a new instance of this fragment for the given section number.
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    // ----- METHODS -----

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        int section = getArguments().getInt(ARG_SECTION_NUMBER);

        switch (section) {
            case 0:
                rootView = inflater.inflate(R.layout.fragment_tabs_resources, container, false);
                makeResourcesSetup();
                break;
            case 1:
                rootView = inflater.inflate(R.layout.fragment_tabs_render, container, false);
                makeRenderSetup();
                break;
            case 2:
                rootView = inflater.inflate(R.layout.fragment_tabs_results, container, false);
                makeResultsSetup();
                break;
        }

        return rootView;
    }

    // ------------------------------ OTHER METHODS ------------------------------

    private void makeRenderSetup() {
        // PAGE 1
        start = (FloatingActionButton) rootView.findViewById(R.id.start);
        delete = (FloatingActionButton) rootView.findViewById(R.id.delete);

        search = (ImageView) rootView.findViewById(R.id.searchImage);
        searchBack = (ImageView) rootView.findViewById(R.id.backSearchImage);
        filename = (TextView) rootView.findViewById(R.id.filename);

        progress = (ProgressBar) rootView.findViewById(R.id.progress);
        percentage = (TextView) rootView.findViewById(R.id.percentage);
        logger = (TextView) rootView.findViewById(R.id.logger);
        finalImage = (ImageView) rootView.findViewById(R.id.finalImage);

        verifyRendering();
    }

    private void makeSearchSetup() {
        // UI THREAD
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            // Delete file
            chosenFile = null;
            result = null;
            filecontents = "";

            filename.setVisibility(View.VISIBLE);
            filename.setText("Select a file");
            filename.setTextColor(getResources().getColor(R.color.colorGray));

            // Hide start button
            start.setVisibility(View.INVISIBLE);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            // Hide delete button
            delete.setVisibility(View.INVISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            // Change button color to gray
            searchBack.setVisibility(View.VISIBLE);
            searchBack.setBackgroundResource(R.drawable.circle_button_gray);

            // Change button image
            search.setVisibility(View.VISIBLE);
            search.setImageResource(R.drawable.ic_search);
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFileChooser();
                }
            });

            // Clean image
            finalImage.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void makeProcessSetup(final String name) {
        // UI THREAD
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            // Change button color to blue
            searchBack.setVisibility(View.VISIBLE);
            searchBack.setBackgroundResource(R.drawable.circle_button_blue);

            // Change button image
            search.setVisibility(View.VISIBLE);
            search.setImageResource(R.drawable.ic_file_white);
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            // Change text color to blue
            filename.setVisibility(View.VISIBLE);
            filename.setText(name);
            filename.setTextColor(getResources().getColor(R.color.colorPrimary));

            // Show start button
            start.setVisibility(View.VISIBLE);
            start.setImageResource(R.drawable.ic_play);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startRendering();
                }
            });

            // Show delete button
            delete.setVisibility(View.VISIBLE);
            delete.setImageResource(R.drawable.ic_delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelChoice();
                }
            });
            }
        });
    }

    private void makeRenderingProcess() {
        // UI THREAD
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            // Hide start button
            start.setVisibility(View.INVISIBLE);

            // Change delete action
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancelRendering();
                }
            });

            // Hide search button
            searchBack.setVisibility(View.INVISIBLE);
            search.setVisibility(View.INVISIBLE);

            // Hide filename text
            filename.setVisibility(View.INVISIBLE);

            // Show progress
            progress.setVisibility(View.VISIBLE);
            percentage.setVisibility(View.VISIBLE);
            percentage.setText("0,0%");

            // Show logger
            logger.setVisibility(View.VISIBLE);
            logger.setText("");
            }
        });
    }

    private void makeViewProcess(final Bitmap b) {
        // UI THREAD
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            // Show start button
            start.setVisibility(View.VISIBLE);
            start.setImageResource(R.drawable.ic_save);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveImage();
                }
            });

            // Change delete usage
            delete.setVisibility(View.VISIBLE);
            delete.setImageResource(R.drawable.ic_delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ignoreImage();
                }
            });

            // Hide progress
            progress.setVisibility(View.INVISIBLE);
            percentage.setVisibility(View.INVISIBLE);

            // Hide logger
            logger.setVisibility(View.INVISIBLE);

            // Show final result image
            finalImage.setVisibility(View.VISIBLE);
            finalImage.setImageBitmap(b);
            }
        });
    }

    private void makeAfterSaveProcess() {
        // UI THREAD
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
            // Show start button
            start.setVisibility(View.VISIBLE);
            start.setImageResource(R.drawable.ic_back);
            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makeSearchSetup();
                }
            });

            // Change delete usage
            delete.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void makeResourcesSetup() {
        // PAGE 2
        support = (FloatingActionButton) rootView.findViewById(R.id.support);
        support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshFiles();
            }
        });

        foldername = (TextView) rootView.findViewById((R.id.folder));
        grid = (GridView) rootView.findViewById(R.id.gridview);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                goToFolder(position);
            }
        });
    }

    private void makeResultsSetup() {
        // PAGE 3
        local_support = (FloatingActionButton) rootView.findViewById(R.id.support);
        local_support.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshImages();
            }
        });

        local_grid = (GridView) rootView.findViewById(R.id.gridview);
    }

    // ----------------------------------------------------------------
    // ---------------------------- RENDER ----------------------------
    // ----------------------------------------------------------------

    // ---------------------------- ACTIONS ----------------------------

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(Intent.createChooser(intent, "Select a file to upload"), SHOW_FILE_CHOOSER);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Snackbar.make(rootView, "Please install a File Manager", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }

    private void cancelChoice() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            makeSearchSetup();
            }
        });
        work.start();
    }

    private void verifyRendering() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            // Verify current status
            status = Menu.server.getStatus(Menu.guid);

            switch(status) {
                case 0:
                    showMessage("Server is prepared");
                    makeSearchSetup();
                    break;
                case 1:
                    showMessage("Resuming rendering process");
                    catchRendering(false);
                    break;
                case 2:
                    showMessage("File was rendered previously");
                    endRendering();
                    break;
                case 3:
                    showMessage("There was an error with the previous rendering");
                    stopRendering();
                    break;
            }
            }
        });
        work.start();
    }

    private void startRendering() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
                showMessage("Starting rendering process");
                catchRendering(true);
            }
        });
        work.start();
    }

    private void catchRendering(boolean render) {
        keepRendering = true;
        makeRenderingProcess();

        try {
            if (render)
                Menu.server.startRender(Menu.guid, filecontents);

            status = Menu.server.getStatus(Menu.guid);
            timings = Menu.server.getTimeRemaining(Menu.guid);

            while (status == 1 && keepRendering) {

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                status = Menu.server.getStatus(Menu.guid);
                timings = Menu.server.getTimeRemaining(Menu.guid);

                // UI THREAD
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    long lf = Math.round(timings.getFirst());
                    long ls = Math.round(timings.getSecond());

                    if (lf != -1L && ls != -1L) {
                        time = timings.getFirst();
                        double p = (time / (time + timings.getSecond())) * 100;

                        percentage.setText(String.format("%1$,.1f", p) + "%");
                        logger.setText("> Rendering [" + lf + "s | " + ls + "s]");
                    } else {
                        percentage.setText("100%");
                        logger.setText("> Done in " + String.format("%1$,.1f", time) + "s");
                    }
                    }
                });

            }

            if (status == 2 && keepRendering) {
                // End rendering process
                showMessage("Ending rendering process");
                endRendering();
            } else {
                showMessage("Syntax error or rendering process status lost");
                makeProcessSetup(chosenName);
            }

        } catch (IOException e) {
            showMessage("IOException thrown, can't handle file");
        }
    }

    private void endRendering() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            // UI THREAD
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    delete.setVisibility(View.INVISIBLE);
                }
            });

            showMessage("Fetching result image");

            try {
                result = Menu.server.getRenderedImage(Menu.guid);

                // Convert image
                showMessage("Converting image to Bitmap");
                chosenBitmap = BitmapFactory.decodeByteArray(result, 0, result.length);

                // Load image
                showMessage("Loading result image");
                makeViewProcess(chosenBitmap);

            } catch (IOException i) {
                showMessage("There was an error fetching the result");
                cancelRendering();
            }
            }
        });
        work.start();
    }

    private void cancelRendering() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            showMessage("Cancelling rendering process");
            keepRendering = false;
            stopRendering();

            // UI THREAD
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                // Show start button
                start.setVisibility(View.VISIBLE);

                // Show search button
                searchBack.setVisibility(View.VISIBLE);
                search.setVisibility(View.VISIBLE);

                // Show filename text
                filename.setVisibility(View.VISIBLE);

                // Hide progress
                progress.setVisibility(View.INVISIBLE);
                percentage.setVisibility(View.INVISIBLE);

                // Hide logger
                logger.setVisibility(View.INVISIBLE);
                }
            });

            }
        });
        work.start();
    }

    private void stopRendering() {
        Menu.server.stopRendering(Menu.guid);
    }

    private void saveImage() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            // UI THREAD
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Image name");

                // Set up the input
                final EditText input = new EditText(getActivity());

                // Specify the type of input expected
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    work = new Thread(new Runnable() {
                        @Override
                        public void run() {
                        String tempName = input.getText().toString();
                        if (!tempName.equals("")) {
                            showMessage("Saving image to DroidTracer folder");

                            String root = Environment.getExternalStorageDirectory().toString();
                            File myDir = new File(root + "//DroidTracer//");
                            myDir.mkdirs();

                            File file = new File(myDir, tempName + ".jpg");

                            if (file.exists())
                                file.delete();

                            try {
                                FileOutputStream out = new FileOutputStream(file);
                                chosenBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
                                out.flush();
                                out.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            showMessage("Result successfully saved as " + tempName + ".jpg");

                            makeAfterSaveProcess();
                        } else {
                            showMessage("Name can't be blank");
                        }
                        }
                    });
                    work.start();
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                }
            });
            }
        });
        work.start();
    }

    private void ignoreImage() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            showMessage("Ignoring fethed file");
            makeSearchSetup();
            }
        });
        work.start();
    }

    // ---------------------------- AUXILIARY METHODS ----------------------------

    private String getRealPathFromURI(Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(rootView.getContext(), uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private String getDataColumn(Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = rootView.getContext().getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private String extractName(String s) {
        StringTokenizer st = new StringTokenizer(s, "/");
        String token = "";

        while(st.hasMoreTokens()){
            token = st.nextToken();
        }

        return token;
    }

    private String extractExtension(String s) {
        StringTokenizer st = new StringTokenizer(s, ".");
        String token = "";

        while(st.hasMoreTokens()){
            token = st.nextToken();
        }

        return token;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }

        reader.close();
        return sb.toString();
    }

    private String getStringFromFile (File fl) throws IOException {
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    // ----------------------------------------------------------------
    // ---------------------------- RESOURCES -------------------------
    // ----------------------------------------------------------------

    // ---------------------------- ACTIONS ----------------------------
    private void refreshFiles() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            showMessage("Refreshing resources list");
            path = new ArrayList<>();

            // FETCH RESOURCES FROM SERVER
            root = Menu.server.listResources(Menu.guid);

            // UI THREAD
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                foldername.setText("../" + root.getName());
                grid.setAdapter(new FileAdapter(rootView.getContext(),root));
                }
            });
            }
        });
        work.start();
    }

    private void goBack() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            int size = path.size();
            int lastindex = size - 1;
            int newlastindex = size - 2;

            path.remove(lastindex);
            if (newlastindex < 0) {
                // UI THREAD
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    foldername.setText("../" + root.getName());
                    grid.setAdapter(new FileAdapter(rootView.getContext(), root));
                    support.setImageResource(R.drawable.ic_reload);
                    support.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            refreshFiles();
                        }
                    });
                    }
                });
            } else {
                int newlastvalue = path.get(newlastindex);
                path.remove(newlastindex);

                goToFolder(newlastvalue);
            }
            }
        });
        work.start();
    }

    private void goToFolder(int position) {
        final int pf = position;
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            final FileTreeNode temp = calculateFolder(pf);

            if (temp != null) {
                // UI THREAD
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    foldername.setText("../" + temp.getName());
                    grid.setAdapter(new FileAdapter(rootView.getContext(), temp));
                    support.setImageResource(R.drawable.ic_back);
                    support.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            goBack();
                        }
                    });
                    }
                });
            }
            }
        });
        work.start();
    }

    // ---------------------------- AUXILIARY METHODS ----------------------------

    private FileTreeNode calculateFolder(int position) {
        FileTreeNode res = root;
        path.add(position);

        res = root;
        for (int i : path) {
            res = res.getNodes().get(i);
        }

        if (res.isFolder())
            return res;
        else {
            int size = path.size();
            int lastindex = size - 1;
            path.remove(lastindex);

            return null;
        }
    }

    // ----------------------------------------------------------------
    // ---------------------------- RESULTS -------------------------
    // ----------------------------------------------------------------

    // ---------------------------- ACTIONS ----------------------------

    private void refreshImages() {
        work = new Thread(new Runnable() {
            @Override
            public void run() {
            showMessage("Refreshing local images list");

            // FETCH RESOURCES FROM SERVER
            local_root = listImages();

            // UI THREAD
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                local_grid.setAdapter(new ImageAdapter(getActivity(), rootView.getContext(), local_root));
                }
            });
            }
        });
        work.start();
    }

    // ---------------------------- AUXILIARY METHODS ----------------------------

    public ArrayList<String> listImages() {
        ArrayList<String> res = new ArrayList<>();

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "//DroidTracer//");
        myDir.mkdirs();

        File[] files = myDir.listFiles();

        for(File file : files)
            res.add(file.getAbsolutePath());

        return res;
    }

    // ---------------------------- ACTIVITY RETURN METHODS ----------------------------

    @Override
    public void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        switch (requestCode) {
            case SHOW_FILE_CHOOSER:
                if (resultCode == RESULT_OK) {
                    work = new Thread(new Runnable() {
                        @Override
                        public void run() {
                        try {
                            // Get the URI of the selected file
                            Uri uri = data.getData();
                            chosenPath = getRealPathFromURI(uri);
                            chosenName = extractName(chosenPath);
                            chosenExtension = extractExtension(chosenName);

                            chosenFile = new File(chosenPath);

                            if (chosenExtension.equals("pbrt")) {

                                // PROCESS RENDERING SETUP
                                makeProcessSetup(chosenName);

                                // EXTRACT FILE CONTENTS
                                try {
                                    filecontents = getStringFromFile(chosenFile);
                                    showMessage("File contents fetch successfully");

                                } catch (IOException e) {
                                    showMessage("Not authorized to fetch file contents");
                                }

                            } else {
                                showMessage("Format must be '.pbrt', found '." + chosenExtension + "' instead");
                            }

                        } catch (NullPointerException n) {
                            showMessage("Error parsing file URI path");
                        }
                        }
                    });
                    work.start();
                } else {
                    showMessage("Error selecting file");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showMessage(String text) {
        Snackbar.make(rootView, text, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
