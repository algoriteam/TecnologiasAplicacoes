// --------------------------------------- CLASSES --------------------------------------- //

----- [GENERATOR] -----

-> void drawFunction(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax, float voxelSize, int function, float isoLevel);

- Esta � a fun��o que deve ser chamada pelo utilizador na plataforma. Este m�todo muito simples trata apenas de criar uma "GRID" (aka GRELHA) que ocupa um certo espa�o
e dom�nio que vai desde [xmin->xmax][ymin->ymax][zmin->zmax] e onde cada VOXEL ir� apresentar um tamanho de [voxelSize * voxelSize * voxelSize]. Outros par�metros que s�o 
passados a esta fun��o determinam qual a fun��o a apresentar:

	1 - Esfera - X*X + Y*Y + Z*Z - 16.0f
	
2 - Random1 - sinf(X*Y + X*Z + Y*Z) + sinf(X*Y) + sinf(Y*Z) + sinf(X*Z) - 1.0f)

e ainda o "isolevel" que se trata de um par�metro que permite determinar aquilo que queremos que seja vis�vel da superficie em quest�o (deve ser ajustado consoante ambiente
e superficie a desenhar);

- Durante a execu��o desta fun��o, uma GRID � criada e todas as posi��es dos voxeis s�o geradas (grid.generateVoxelPositions()). Se tudo correr bem e o dom�nio de gera��o destes
v�xeis for poss�vel, a fun��o/superficie � desenhada (grid.drawFunction());

----- [GRID] -----

-> bool generateVoxelPositions();

- Trata - se de um m�todo que gera todos os v�xeis existentes na grelha de dom�nio XYZ escolhido (Guardar na estrutura da GRID todos os VOXEIS);

-> void drawFunction();

- Este m�todo divide - se em 4 FASES:

	1 - Para cada voxel existente, calcular os pontos dos seus v�rtices (Guardar na estrutura do VOXEL essas posi��es - [(X, Y, Z)]);
	2 - Para cada voxel existente, calcular a fun��o para cada um dos v�rtices gerados (Guardar na estrutura do VOXEL esses valores - [(FLOAT)]);
	3 - Executar o m�todo "polygonize" que se trata do algoritmo dos "Marching Cubes", onde basicamente vamos tentar descobrir quais os v�rtices que se encontram "dentro
	    e fora" da superf�cie, sendo esta propriedade controlada pelo par�metro do "isoLevel". Consoante a disposi��o e conjunto de v�rtices que se encontram dentro da superficie,
	    o m�todo trata de realizar uma interpola��o linear desses v�rtices ("Descobrir o v�rtice interm�dio") e traduz este conjunto de v�rtices numa lista de �ndices (Tri�ngulos a
	    desenhar);
	4 - Inicialmente, cri�mos a estrutura CELL, onde obrig�vamos a aplica��o a percorrer todos os v�xeis e desenhar um de cada vez, o que no fim demonstrou - se ineficiente;
	    De forma a comatar esta quest�o cri�mos uma outra estrutura MESH, onde realiz�mos o "merge" de todos os v�rtices e �ndices calculados dos tri�ngulos a desenhar. Ou seja,
	    agreg�mos tudo numa �nica mesh de forma a desenhar um �nico objeto, mais complexo, o que veio mostrar ser muito mais eficiente.

----- [VOXEL] -----

-> FVector getPosition();

- Devolve a posi��o CENTRAL do voxel;

-> bool hasSomething();

- Verifica se o voxel, ap�s executar o algoritmo dos "Marching Cubes" (polygonize()), tem algum triangulo a desenhar;

-> TArray<FVector> getVertices();

- Devolve a lista dos v�rtices do CUBO representado pelo VOXEL;

-> TArray<FVector> getNewVertices();

- Devolve a lista dos novos v�rtices ap�s executar o algoritmo dos "Marching Cubes" (polygonize());

-> TArray<int32> getTriangles();

- Devolve a lista dos �ndices dos v�rtices de forma a desenhar e representar os tri�ngulos;

-> void calculateVertices();

- Calcula os v�rtices do CUBO, partido apenas da posi��o CENTRAL do VOXEL;

-> void calculateFunction();

- Calcula o valor da fun��o escolhida para cada um dos v�rtices do CUBO representado pelo VOXEL;

-> bool polygonize();

- M�todo que realiza o algoritmo dos "Marching Cubes";

-> FVector interpolate(const FVector &p1, const FVector &p2, float valp1, float valp2);

- M�todo que realiza a interpola��o linear entre dois pontos XYZ, tendo em conta o valor da FUN��O nessas mesmas posi��es;

-> float sphereFunction(FVector v);

- Fun��o da esfera;

-> float randomFunction1(FVector v);

- Uma qualquer fun��o random;

----- [MESH] -----

-> void setup(TArray<FVector> vertices, TArray<int32> triangles);

- Dentro do engine do unrealEngine 4 existe um m�todo denominado "CreateMeshSection_LinearColor" que para uma dada propriedade do objeto (UProceduralMeshComponent) cria uma mesh a partir da lista de v�rtices, indices, normais, UV0, cores dos v�rtices, tangentes, etc ...

// --------------------------------------- EXTRA CLASSES --------------------------------------- //

----- [CELL] -----

-> void setup(TArray<FVector> vertices, TArray<int32> triangles);

- Dentro do engine do unrealEngine 4 existe um m�todo denominado "CreateMeshSection_LinearColor" que para uma dada propriedade do objeto (UProceduralMeshComponent) cria uma mesh a partir da lista de v�rtices, indices, normais, UV0, cores dos v�rtices, tangentes, etc ...

// --------------------------------------- HOW DOES IT WORK --------------------------------------- //