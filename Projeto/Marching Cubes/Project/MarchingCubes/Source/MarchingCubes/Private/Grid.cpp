// Fill out your copyright notice in the Description page of Project Settings.

#include "MarchingCubes.h"
#include "Grid.h"

Grid::Grid()
{
}

Grid::Grid(UWorld *w,float xi, float xm, float yi, float ym, float zi, float zm, float vs, int f, float iso)
{
	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Creating grid ..."));

	// ----- SET VARIABLES ----- //

	world = w;

	xmin = xi;
	xmax = xm;
	ymin = yi;
	ymax = ym;
	zmin = zi;
	zmax = zm;

	voxelSize = vs;

	function = f;
	isoLevel = iso;
}

Grid::~Grid()
{
}

// ----- OTHER METHODS ----- //

void Grid::drawFunction() {
	FColor blue = FColor(0, 0, 100);
	FColor green = FColor(0, 100, 0);
	FColor red = FColor(100, 0, 0);

	TArray<FVector> vertices;
	TArray<int32> triangles;

	float half_voxelSize = voxelSize * 0.5f;

	// ----- CHECK ALL CELLS ----- //
	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Checking voxels ..."));
	int accumulated = 0;

	int i = 0;
	int j = 0;
	for (Voxel v : voxels) {

		// Calculate vertices
		v.calculateVertices();

		// Calculate function
		v.calculateFunction();

		// Polygonize
		v.polygonize();

		// ----- SLOWER - ONE MESH per VOXEL ----- //
		// if (v.hasSomething()) {
		//   ACell *cell = world->SpawnActor<ACell>(ACell::StaticClass(), FVector::ZeroVector , FRotator::ZeroRotator);
		//	 cell->setup(v.getNewVertices(), v.getTriangles());
		// }

		// ----- FASTER - MERGED MESH ----- //
		if (v.hasSomething()) {
			TArray<FVector> tempV = v.getNewVertices();
			TArray<int32> tempT = v.getTriangles();

			for (int32 b : tempT) {
				triangles.Add(b + i);
				// UE_LOG(LogTemp, Warning, TEXT("Triangle[%d] = %d"), j, triangles[j]);

				j++;
			}

			for (FVector a : tempV) {
				vertices.Add(a);
				// UE_LOG(LogTemp, Warning, TEXT("Vertice[%d] = %.2f %.2f %.2f"), i, a.X, a.Y, a.Z);

				i++;
			}
		}

	}
	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] All possible voxels checked and processed ..."));

	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Drawing final mesh ..."));
	AMesh *mesh = world->SpawnActor<AMesh>(AMesh::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator);
	mesh->setup(vertices, triangles);
	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Final mesh drawn ..."));
}

bool Grid::generateVoxelPositions() {
	float diffX = xmax - xmin;
	float diffY = ymax - ymin;
	float diffZ = zmax - zmin;

	UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Generating voxel positions ..."));

	// ----- VERIFY ----- //
	if (diffX <= voxelSize || diffX < 0 || diffY <= voxelSize || diffY < 0 || diffZ <= voxelSize || diffZ < 0) {
		UE_LOG(LogTemp, Warning, TEXT("[ERROR] Invalid parameters for the limits ..."));
		return false;
	}
	else {
		float stepX;
		float stepY;
		float stepZ;
		float half_size = voxelSize * 0.5f;

		stepX = xmin + half_size;
		for (int i = 0; stepX + voxelSize < xmax; i++) {
			stepX = xmin + half_size + voxelSize * i;

			stepY = ymin + half_size;
			for (int j = 0; stepY + voxelSize < ymax; j++) {
				stepY = ymin + half_size + voxelSize * j;

				stepZ = zmin - half_size;
				for (int k = 0; stepZ + voxelSize < zmax; k++) {
					stepZ = zmin + half_size + voxelSize * k;
					voxels.Add(Voxel(FVector(stepX, stepY, stepZ), voxelSize, function, isoLevel));
				}

			}

		}

		UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] All voxel positions generated ..."));
		return true;
	}
}
