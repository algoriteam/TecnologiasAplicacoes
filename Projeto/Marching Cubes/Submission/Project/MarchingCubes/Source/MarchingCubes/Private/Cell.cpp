// Fill out your copyright notice in the Description page of Project Settings.

#include "MarchingCubes.h"
#include "Cell.h"

// ----- DEFAULT METHODS ----- //

// Sets default values
ACell::ACell()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Creating cell ..."));

	// ----- CREATE MESH ----- //
	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("GeneratedMesh"));
	RootComponent = mesh;
}

// Called when the game starts or when spawned
void ACell::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// ----- OTHER METHODS ----- //

void ACell::setup(TArray<FVector> vertices, TArray<int32> triangles)
{
	// UE_LOG(LogTemp, Warning, TEXT("[GENERATOR] Setting up cell ..."));

	// ----- ASSIGN MESH VALUES ----- //
	/**
	*	Create/replace a section for this procedural mesh component.
	*	@param	SectionIndex		Index of the section to create or replace.
	*	@param	Vertices			Vertex buffer of all vertex positions to use for this mesh section.
	*	@param	Triangles			Index buffer indicating which vertices make up each triangle. Length must be a multiple of 3.
	*	@param	Normals				Optional array of normal vectors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	UV0					Optional array of texture co-ordinates for each vertex. If supplied, must be same length as Vertices array.
	*	@param	VertexColors		Optional array of colors for each vertex. If supplied, must be same length as Vertices array.
	*	@param	Tangents			Optional array of tangent vector for each vertex. If supplied, must be same length as Vertices array.
	*	@param	bCreateCollision	Indicates whether collision should be created for this section. This adds significant cost.
	*/
	TArray<FVector> normals;
	TArray<FVector2D> UV0;
	TArray<FLinearColor> vertexColors;
	TArray<FProcMeshTangent> tangents;

	mesh->CreateMeshSection_LinearColor(1, vertices, triangles, normals, UV0, vertexColors, tangents, false);
}
