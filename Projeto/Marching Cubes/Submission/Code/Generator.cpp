// Fill out your copyright notice in the Description page of Project Settings.

#include "MarchingCubes.h"
#include "Generator.h"

// ----- DEFAULT METHODS ----- //

// Sets default values
AGenerator::AGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGenerator::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGenerator::drawFunction(float xmin, float xmax, float ymin, float ymax, float zmin, float zmax, float voxelSize, int function, float isoLevel) {

	// ----- CREATE GRID ----- //
	UWorld *world = GetWorld();
	Grid grid = Grid(world, xmin, xmax, ymin, ymax, zmin, zmax, voxelSize, function, isoLevel);

	// ----- GENERATE VOXELS ----- //
	bool generated = grid.generateVoxelPositions();

	// ----- DRAW ALL VOXELS ----- //
	if (generated) {
		grid.drawFunction();
	}
}

